





The function used in my notebooks are from https://github.com/GustavoRP/DTIlib
This module has functions to read, manipulate, and to compute maps in DTI.


# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for IA369Z class at FEEC-UNICAMP (2017/1).

/dev contains the codes being developed.
/deliver contains notebooks that are finished and my own libraries.
/figures contains the images to the executable paper.
/data is for the project dats. It contains NIFTI .

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact